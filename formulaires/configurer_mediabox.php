<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('mediabox_pipelines');

function box_lister_skins($motif='colorbox') { // colorbox, lib historique

	$skins = array();

	if ($motif !== 'colorbox') {
		// normaliser le terme de recherche comme un nom de dossier
		$motif = mediabox_slugify($motif);
		if ( !(is_string($motif)) or strlen($motif) > 60 or strlen($motif) < 5 ) {
			spip_log('box_lister_skins : motif de recherche inadapté, retour à valeur par défaut' , 'mediabox.' . _LOG_ERREUR);
			$motif = 'colorbox';
		}
	}

	$maxfiles = 1000;
	$liste_fichiers = array();
	$recurs = array();
	foreach (creer_chemin() as $d) {
		$f = $d . $motif . "/";
		if (@is_dir($f)) {
			$liste = preg_files($f, $motif . "[.]css([.]html)?$", $maxfiles - count($liste_fichiers), $recurs);
			foreach ($liste as $chemin) {
				$nom = substr(dirname($chemin), strlen($f));
				// ne prendre que les fichiers pas deja trouves
				// car find_in_path prend le premier qu'il trouve,
				// les autres sont donc masques
				if (!isset($liste_fichiers[$nom])) {
					$liste_fichiers[$nom] = $chemin;
				}
			}
		}
	}
	foreach ($liste_fichiers as $short => $fullpath) {
		$skins[$short] = array('nom' => basename($short) , 'lib' => $motif); // conserver l'info 
		if (file_exists($f = dirname($fullpath) . "/vignette.jpg")) {
			$skins[$short]['img'] = $f;
		}
	}

	return $skins;
	
}

function box_choisir_skin($skins, $selected, $name = 'skin') {
	$out = "";
	if (!is_array($skins) or !count($skins)) {
		return $out;
	}
	foreach ($skins as $k => $skin) {
		$id = "${name}_" . preg_replace(",[^a-z0-9_],i", "_", $k);
		$sel = ($selected == "$k" ? " checked='checked'" : '');
		$balise_img = chercher_filtre('balise_img');
		$lib = $skin['lib'];
		if (isset($skin['img'])) {
			$label = $balise_img($skin['img'],$skin['nom']);
			$label .= '<a href="' . $skin['img'] . '" class="popin" rel="habillage">'. $skin['nom'].'</a>';
		}
		else {
			$label = $skin['nom'];
		}
		$out .= "<div class='choix'>";
		$out .= "<input type='radio' name='$name' id='$id' value='$k'$sel data-lib='$lib' />";
		$out .= "<label for='$id'>$label</label>";
		$out .= "</div>\n";
	}

	return $out;
}


function formulaires_configurer_mediabox_charger_dist() {
	$valeurs = mediabox_config(true);

	//liste simplifiée
	$libs = array();
	foreach ($valeurs['_libs'] as $k => $v) {
		$libs[] = $k;
	}
	//lister les skins pour chaque lib
	$skins = array();
	foreach ($libs as $lib) {
		$skins = array_merge($skins, box_lister_skins($lib));
	}
	$valeurs['_skins'] = $skins;

	return $valeurs;
}

function formulaires_configurer_mediabox_traiter_dist() {
	$config = mediabox_config(true);

	include_spip('inc/meta');
	if (_request('reinit')) {
		foreach ($config as $k => $v) {
			set_request($k);
		}
		effacer_config('mediabox');
	} else {
		// cas particulier de la checkbox :
		if (!_request('active')) {
			set_request('active', 'non');
		}
		foreach ($config as $k => $v) {
			if (!is_null(_request($k))) {
				$config[$k] = _request($k);
			}
		}	

		// préférences explicites du theme
		if (include_spip( $config['lib'] . '/' . $config['skin'] . '/mediabox_config_theme')) {
			$config_theme = mediabox_config_theme();
			$config = array_merge($config,$config_theme);
		}

		// on recharge sans enregistrer pour mettre à jour les options propre à la skin
		if (_request('recharger')) {
			return ;
		}
		ecrire_config('mediabox', $config);
	}

	include_spip('inc/invalideur');
	suivre_invalideur('mediabox');

	return array('message_ok' => _T('config_info_enregistree'), 'editable' => true);
}
