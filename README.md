# Mediabox v2, l'API de boîte multimedia de SPIP
(Fork depuis v1.0.2)

status : test

Mediabox est un plugin qui normalise l'intégration de plugins jQuery de boîte modale (modalbox / popin) pour SPIP.
Pour l'heure, 2 sous-plugins implémentent cette version 2 :
- [colorbox](https://framagit.org/cantal-tech/colorbox) (lib historique)
- [featherlight](https://framagit.org/cantal-tech/featherlight)

## Objectifs de la v2 : 
- Achever le découpage entre la déclaration de l'API et son implémentation par les sous-plugins. 
- Rendre Mediabox indépendante de colorbox (modalbox historique) 
- Faciliter la surcharge en ayant recours à des fichiers ciblés.
- Automatiser certaines actions récurrentes (ex. : insertion des scripts via insert_head, ...)
- Normaliser l'usage à l'attention des intégrateurs via les attributs `data-href-popin` (qui contient un href alternatif pour la modal) et `data-box-type` (qui précise le type de contenu).

## Nouveautés :
- un pipeline **mediabox_config** que les sous-plugins peuvent enrichir de leurs propres paramètres.
- les skins peuvent désormais être des fichiers statiques (colorbox.css) ou des **fichiers dynamiques** (colorbox.css.html)
- les préférences de l'utilisateur sont issues d'un fichier dynamique (box_settings.js.html) et donc plus aisemment surchargeables.
- un nouveau paramètre "**namespace**" qui fait (en outre) office de préfixe pour les attributs (ex: data-box-type) Sa valeur est 'box' par défaut. Peut être surdéfinit en fonction de la skin choisie pour compatibilté montante avec la lib sélectionnée (ex : 'cbox' pour colorbox).
- le formulaire configurer_mediabox présente **une interface de sélection de skins unifiée** où se cotoient les skins issues de différents sous-plugins (désignés 'libs'). 
- une **page de documentation** propose des cas d'usage de la syntaxe (en squelettes et en jQuery).
- une nouvelle fonction `mediabox_autodetect_href` facilite **la détection du type de contenu** (inline, image, ajax, iframe, ...) à afficher dans la mediabox (on peut cependant forcer la valeur en dur avec `data-box-type="iframe"` en squelette, ou encore `$.modalboxload('https://domaine.tld/page', {type:'iframe'})` en jQuery.


## NB :
- embarque une surcharge de inc-confirmer_actions.html (plugin-dist SVP) pour appeler l'API sans référence à colorbox

## TODO's :
- Intégrer les nouvelles chaines de langues
- Gérer l'upgrade de la configuration depuis v1