<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/mediabox/lang/

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
// 'autodetection_type' => 'Auto détection du type',

// B
	'bouton_recharger' => 'Recharger',
	'bouton_reinitialiser' => 'Réinitialiser',
	'boxstr_close' => 'Fermer',
	'boxstr_current' => '@current@/@total@',
	'boxstr_next' => 'Suivant',
	'boxstr_previous' => 'Précédent',
	'boxstr_slideshowStart' => 'Diaporama',
	'boxstr_slideshowStop' => 'Arrêter',
	'boxstr_zoom' => 'Zoom',

// E
	'explication_selecteur' => 'Indiquez la cible des éléments qui déclencheront la boîte. (Expression CSS ou étendue jQuery)',
	'explication_selecteur_galerie' => 'Indiquez la cible des éléments à regrouper en galerie. (Expression CSS ou étendue jQuery)',
	'explication_splash_url' => 'Indiquez l’url du média à afficher automatiquement dans une boîte lors de la première visite sur le site public.',
	'explication_traiter_toutes_images' => 'Insérer une boîte sur toutes les images ?',
// I
	'info_image_html' => 'La class <em>mediabox</em> n\'est pas nécessaire si l\'option de traitement automatique des images a été cochée dans la configuration',
	'info_inline_html' => 'On peut cibler un élément du DOM (class | id) .',
	'info_html_html' => 'La cible peut-être du langage HTML balisé. Usage à réserver à des éléments courts.',
	'info_ajax_html' => 'Permet l\'affichage d\'éléments venant d\'autres pages du site (ressources statiques, modèles ,inclures, ...).',
	'info_iframe_html' => 'Permet l\'affichage d\'éléments extérieur au site.',
// L
	'label_active' => 'Activer la Boîte Multimédia dans le site public',
	'label_apparence' => 'Apparence',
	//'label_aucun_style' => 'N’insérer aucun habillage par défaut',
	'label_choix_transition_elastic' => 'Élastique',
	'label_choix_transition_fade' => 'Fondu enchaîné',
	'label_choix_transition_none' => 'Sans effet de transition',
	'label_lib' => 'Sous-plugins',
	'label_maxheight' => 'Hauteur Maxi (% ou px)',
	'label_maxwidth' => 'Largeur Maxi (% ou px)',
	'label_minheight' => 'Hauteur Mini (% ou px)',
	'label_minwidth' => 'Largeur Mini (% ou px)',
	'label_namespace' => 'Namespace',
	'label_opacite' => 'Opacité du fond',
	'label_selecteur_commun' => 'En général',
	'label_selecteur_galerie' => 'En galerie',
	'label_selecteurs' => 'Sélecteurs',
	'label_skin' => 'Habillage visuel',
	'label_slideshow_speed' => 'Temps d’exposition des photos du diaporama (ms)',
	'label_speed' => 'Vitesse de transition (ms)',
	'label_splash' => 'Splash Boîte',
	'label_splash_height' => 'Hauteur (% ou px)',
	'label_splash_url' => 'URL à afficher',
	'label_splash_width' => 'Largeur (% ou px)',
	'label_traiter_toutes_images' => 'Images',
	'label_transition' => 'Transition entre deux affichages',

// O
	'onglet_mediabox_html' => 'Syntaxe SPIP',
	'onglet_mediabox_js' => 'API Javascript',
	'ouverture_via_api' => 'Ouverture via API',

// P
	'pose_ecouteur_evenement' => 'Poser un écouteur d\'événements',
	'plugins_complementaires_texte' => 'Plusieurs sous-plugins peuvent implémenter la mediabox.',
	'plugins_complementaires_titre' => 'Plugins complémentaires',

// T
	'titre_html_exemple' => 'Bonjour !',
	'titre_menu_box' => 'Boîte Multimédia',
	'titre_page_configurer_box' => 'Configuration de la Boîte multimédia',
	'titre_page_mediabox_doc' => 'Documentation de Mediabox',
);
