<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function mediabox_insert_head($flux) {
	$config = mediabox_config();

	if (
		$config['active'] == 'oui' 
		AND $config['lib'] 
		AND count($config['_libs']) 
		) {

		// chargement auto des fichiers déclarés par pipeline mediabox_config();
		$insert_head = $config['_libs'][$config['lib']];

		if ( is_array($insert_head) )  {
			foreach ( $insert_head as $v ) {
				$f = $v['fichier'];
				$f_complet = $v['chemin'].$f;

				// chercher dans le path
				$balise = trouver_ou_produire($f,'',true);

				// sinon dans dir lib
				if (!$balise) {
					$balise = trouver_ou_produire($f_complet,'',true);
				}

				// doit-il rester hors du flux compressé ?
				if ( $balise AND isset($v['hors_compresseur']) ) {
					$balise = inserer_attribut($balise,"id", mediabox_slugify($f));
				}

				$flux .= $balise;
			}
		}

		// fichier d'initialisation de le mediabox
		$flux .= trouver_ou_produire('javascript/spip.mediabox.js','',true);

		// splashbox
		if ($config['splash_url']) {
			$flux .= trouver_ou_produire('javascript/splash.mediabox.js','',true);
		}
		
	}
	return $flux;
}

function mediabox_insert_head_css($flux) {
	$config = mediabox_config();

	if ($config['active'] == 'oui') {

		// box_settings (avant tout le reste)
		$box = produire_fond_statique('javascript/box_settings.js', array('skin' => $config['skin'],'espace_prive' => test_espace_prive() ) );
		if ($box) {
			$flux .= "\n".'<script type="text/javascript" src="'. $box . '" id="box_settings"></script>';		
		}
		// CSS de la skin
		if ($config['skin'] !== 'none') {
			$motif = (test_espace_prive() ? 'prive/' : '') . $config['lib'] . '/' . $config['skin'] . '/' . $config['lib'];
			$flux .= trouver_ou_produire($motif.'.css','',true);		
		}

	}
	
	return $flux;
}

/**
 * Compléter le formulaire de configuration avec un inclure de la lib sélectionnée
 *
 * @param array $flux
 * @return array $flux
 */
function mediabox_formulaire_fond($flux) {
	# formulaire : configurer_mediabox
	if (
		$flux['args']['form'] == 'configurer_mediabox'
		and ($p = strpos($flux['data'], '<!--apparence-->'))
		and $lib = $flux['args']['contexte']['lib']
	) {
		$ajout = recuperer_fond("formulaires/inc-configurer_mediabox-$lib", $flux['args']['contexte']);
		$flux['data'] = substr_replace($flux['data'], $ajout, $p, 0);
	}
	return $flux;
}


function mediabox_jquery_plugins($plugins) {
	$config = mediabox_config();
	if ($config['splash_url']) {
		$plugins[] = 'javascript/js.cookie.js';
	}
	return $plugins;
}

